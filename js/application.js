$(document).ready( function() {
	// console.log("document ready");
	
	// http://stackoverflow.com/questions/654112/how-do-you-detect-support-for-vml-or-svg-in-a-browser
	var hasSVG = document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1");
	// console.log("has svg " + hasSVG);

	if (!hasSVG) { // best way to have beatiful retina logo in SVG
		$('.hz-logo img').each(function() {
			// console.log(this.src);
			this.src = this.src.replace(/.svg/, ".png");
		});
	}


	// add active class to all active anchors
	// var x = "Path name: " + location.pathname;
	// console.log(location.hash);
	var activeElem;
	var x = location.pathname + location.hash;
	// console.log(x);
	$('a[href$=\"' + x + '\"]').each(function (e) {
		// console.log(e.href);
		// activeElem.removeClass("active");
		activeElem = $(this).parent();
		activeElem.addClass("active");
	});


	$('a').on('click',function (e) {
		var x = location.pathname + this.hash;
		// console.log(x);
		$('a[href$=\"' + x + '\"]').each(function (e) {
			activeElem.removeClass("active");
			activeElem = $(this).parent();
			activeElem.addClass("active");
		});
	});

    // $(document).scrollTop( $("#ipo").offset(-120).top ); 
	// $('.hz-about ul li').click(function() {
	// 	  $(this).addClass("active");
	//     var link = $(this).attr("href");
	//     $('#hz-about-content').load("index.html");
	//     return false;
	// });

	$('.navbar-toggle').click(function(ev, ob) {
		$nav = $('#nav');//.removeClass('collapse');
		$nav.toggleClass('collapse');
	    // alert(nav);
		ev.stopPropagation();
	});
});
